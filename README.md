# vc-chord-diagram

Website: [https://vc-chord-diagram.wemakesites.net](https://vc-chord-diagram.wemakesites.net)
Repo: [https://bitbucket.org/acidmartin/vc-chord-diagram](https://bitbucket.org/acidmartin/vc-chord-diagram)

Author: [Martin Ivanov](https://wemakesites.net)

> VcChodDiagram is an open source guitar chord diagram generator for the VueJs JavaScript framework, wrapped as an ES6 module and available on NPM, Yarn and on jsDelivr and UnPkg CDN. It is fully customizable and supports different number of strings and tunings.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

###Demos, documentation and quick start

https://vc-chord-diagram.wemakesites.net
