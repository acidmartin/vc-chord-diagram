/**
 * Social services for the SocialBar component
 * **/
const Social = [{
  icon: 'mdi-twitter',
  url: (url) => {
    return `https://twitter.com/intent/tweet/?text=&url=${url}`
  }
}, {
  icon: 'mdi-facebook',
  url: (url) => {
    return `https://facebook.com/sharer/sharer.php?u=${url}`
  }
}, {
  icon: 'mdi-google-plus',
  url: (url) => {
    return `https://plus.google.com/share?url=${url}`
  }
}, {
  icon: 'mdi-pinterest',
  url: (url) => {
    return `https://pinterest.com/pin/create/button/?url=${url}`
  }
}, {
  icon: 'mdi-linkedin',
  url: (url) => {
    return `https://www.linkedin.com/shareArticle?mini=true&url=${url}`
  }
}, {
  icon: 'mdi-xing',
  url: (url) => {
    return `https://www.xing.com/app/user?op=share;title=;url=${url}`
  }
}, {
  icon: 'mdi-reddit',
  url: (url) => {
    return `https://reddit.com/submit/?url=${url}`
  }
}, {
  icon: 'mdi-tumblr',
  url: (url) => {
    return `https://www.tumblr.com/widgets/share/tool?posttype=link&title=&caption=&shareSource=tumblr_share_button&content=${url}&canonicalUrl=${url}`
  }
}, {
  icon: 'mdi-whatsapp',
  url: (url, siteTitle) => {
    return `whatsapp://send?text=${encodeURIComponent(siteTitle)} ${url}`
  }
}, {
  icon: 'mdi-rss',
  url: () => {
    return `http://feeds.feedburner.com/acidmartin`
  }
}, {
  icon: 'email',
  url: (url, siteTitle) => {
    return `mailto:?subject=${siteTitle}&body=${url}`
  }
}]

export default Social
